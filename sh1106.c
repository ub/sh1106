
#include "sh1106.h"

#include <stdint.h>
#include <string.h>

#include "driver/i2c.h"


#define SH1106_ADDR         0x3C // I2C address
#define SH1106_STREAM       0x00 // last control byte, data bytes to follow; no control packets allowed
#define SH1106_SINGLE       0x80 // control byte, data byte, control byte
#define SH1106_OP_CMD       0x00 // next data byte will be a CMD OP
#define SH1106_OP_RAM       0x40 // next data byte will be a RAM OP


#define SH1106_REG          0xAD // voltage converter mode
#define SH1106_REG_OFF      0x8A
#define SH1106_REG_ON       0x8B // the panel must be OFF while issuing this command

#define SH1106_DISPLAY_OFF  0xAE // sleep mode
#define SH1106_DISPLAY_ON   0xAF

#define SH1106_ENTIRE_OFF   0xA4 // normal operation
#define SH1106_ENTIRE_ON    0xA5 // turn on panel, (priority over SET_REVERSE)

#define SH1106_DCLK         0xD5 // display clock divide ratio and oscillator frequency mode
#define SH1106_DIVIDE_00    0x00 // default: 1; divide ratio
#define SH1106_DIVIDE_01    0x01
#define SH1106_DIVIDE_02    0x02
#define SH1106_DIVIDE_03    0x03
#define SH1106_DIVIDE_04    0x04
#define SH1106_DIVIDE_05    0x05
#define SH1106_DIVIDE_06    0x06
#define SH1106_DIVIDE_07    0x07
#define SH1106_DIVIDE_08    0x08
#define SH1106_DIVIDE_09    0x09
#define SH1106_DIVIDE_10    0x0A
#define SH1106_DIVIDE_11    0x0B
#define SH1106_DIVIDE_12    0x0C
#define SH1106_DIVIDE_13    0x0D
#define SH1106_DIVIDE_14    0x0E
#define SH1106_DIVIDE_15    0x0F
#define SH1106_OSC_FQ_00    0x00 // -25%
#define SH1106_OSC_FQ_01    0x10 // -20%
#define SH1106_OSC_FQ_02    0x20 // -15%
#define SH1106_OSC_FQ_03    0x30 // -10%
#define SH1106_OSC_FQ_04    0x40 // -5%
#define SH1106_OSC_FQ_05    0x50 // default: 0%
#define SH1106_OSC_FQ_06    0x60 // +5%
#define SH1106_OSC_FQ_07    0x70 // +10%
#define SH1106_OSC_FQ_08    0x80 // +15%
#define SH1106_OSC_FQ_09    0x90 // +20%
#define SH1106_OSC_FQ_10    0xA0 // +25%
#define SH1106_OSC_FQ_11    0xB0 // +30%
#define SH1106_OSC_FQ_12    0xC0 // +35%
#define SH1106_OSC_FQ_13    0xD0 // +40%
#define SH1106_OSC_FQ_14    0xE0 // +45%
#define SH1106_OSC_FQ_15    0xF0 // +50%

#define SH1106_DCPC_MODE    0xD9 // pre-dis/charge mode
#define SH1106_PCHARGE_00   0x00 // invalid
#define SH1106_PCHARGE_01   0x01 // 1 CLK pre-charge period adjust
#define SH1106_PCHARGE_02   0x02 // default: 2
#define SH1106_PCHARGE_03   0x03
#define SH1106_PCHARGE_04   0x04
#define SH1106_PCHARGE_05   0x05
#define SH1106_PCHARGE_06   0x06
#define SH1106_PCHARGE_07   0x07
#define SH1106_PCHARGE_08   0x08
#define SH1106_PCHARGE_09   0x09
#define SH1106_PCHARGE_10   0x0A
#define SH1106_PCHARGE_11   0x0B
#define SH1106_PCHARGE_12   0x0C
#define SH1106_PCHARGE_13   0x0D
#define SH1106_PCHARGE_14   0x0E
#define SH1106_PCHARGE_15   0x0F
#define SH1106_DCHARGE_00   0x00 // invalid
#define SH1106_DCHARGE_01   0x10 // 1 CLK dis-charge period adjust
#define SH1106_DCHARGE_02   0x20 // default: 2
#define SH1106_DCHARGE_03   0x30
#define SH1106_DCHARGE_04   0x40
#define SH1106_DCHARGE_05   0x50
#define SH1106_DCHARGE_06   0x60
#define SH1106_DCHARGE_07   0x70
#define SH1106_DCHARGE_08   0x80
#define SH1106_DCHARGE_09   0x90
#define SH1106_DCHARGE_10   0xA0
#define SH1106_DCHARGE_11   0xB0
#define SH1106_DCHARGE_12   0xC0
#define SH1106_DCHARGE_13   0xD0
#define SH1106_DCHARGE_14   0xE0
#define SH1106_DCHARGE_15   0xF0

#define SH1106_CPHC         0xDA // common pads hardware configuration mode
#define SH1106_CPHC_SEQ     0x02 // sequential
#define SH1106_CPHC_ALT     0x12 // default: alternative

#define SH1106_CONTRAST     0x81

#define SH1106_REVERSE_OFF  0xA6
#define SH1106_REVERSE_ON   0xA7

#define SH1106_SEG_REMAP_R  0xA0 // normal direction
#define SH1106_SEG_REMAP_L  0xA1 // reversed direction

#define SH1106_SCAN_L       0xC0 // CAM0  -> CAM63
#define SH1106_SCAN_H       0xC8 // CAM63 -> CAM0

#define SH1106_START_LINE   0x40 // 0x40 - 0x7F

#define SH1106_COLUMN_L     0x00 // 0x00 - 0x0F
#define SH1106_COLUMN_H     0x10 // 0x10 - 0x1F

#define SH1106_PAGE         0xB0


static uint8_t sh1106_pagemask = 0xFF;
static uint8_t sh1106_buffer[8][128] = {0};


void sh1106_init () {
  i2c_cmd_handle_t cmd = i2c_cmd_link_create();

  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, (SH1106_ADDR << 1) | I2C_MASTER_WRITE, true);
  i2c_master_write_byte(cmd, SH1106_STREAM | SH1106_OP_CMD, true);
  i2c_master_write_byte(cmd, SH1106_DISPLAY_OFF, true);
  i2c_master_write_byte(cmd, SH1106_REG, true);
  i2c_master_write_byte(cmd, SH1106_REG_ON, true);
  i2c_master_write_byte(cmd, SH1106_DISPLAY_ON, true);
  i2c_master_write_byte(cmd, SH1106_ENTIRE_OFF, true);
  i2c_master_write_byte(cmd, SH1106_SEG_REMAP_LEFT, true);
  i2c_master_write_byte(cmd, SH1106_SCAN_H, true);
  i2c_master_write_byte(cmd, SH1106_CONTRAST, true);
  i2c_master_write_byte(cmd, 0x00, true);
  i2c_master_stop(cmd);

  i2c_master_cmd_begin(I2C_NUM_0, cmd, 10);
  i2c_cmd_link_delete(cmd);
}


void sh1106_draw_pixel (uint8_t x, uint8_t y) {
  uint8_t segm = x;
  uint8_t page = y >> 3;
  uint8_t line = 0x01 << (y & 7);

  sh1106_buffer[page][segm] |= line;
  sh1106_pagemask |= (0x01 << page);
}


void sh1106_draw_page_rect (uint8_t page_index, uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1) {
  // single page rect
  // x0 < x1 and y0 < y1

  // calculate the byte to draw from x0 to x1
  uitn8_t byte_l = 0xFF << ((y0 & 7));
  uint8_t byte_h = 0xFF << ((y1 & 7) + 1);
  uint8_t byte = byte_l ^ byte_h;

  // copy the line in the buffer and update the pagemask
  for (uint8_t i=x0; i<=x1; ++i)
    sh1106_buffer[page_index][i] = byte;
  sh1106_pagemask |= (0x01 << page_index);
}


void sh1106_draw_hline (uint8_t y) {
  uint8_t page = y >> 3;
  uint8_t line = 0x01 << (y & 7);

  sh1106_pagemask |= (0x01 << page);

  for (uint8_t i=0; i<128; ++i) {
    sh1106_buffer[page][i] |= line;
  }
}


void sh1106_clear_page (uint8_t page_index) {
  memset(sh1106_buffer[page_index], 0x00, 128);
  sh1106_pagemask |= (0x01 << page_index);
}


void sh1106_clear (void) {
  memset(sh1106_buffer, 0x00, 128 << 3);
  sh1106_pagemask = 0x00;
}


void sh1106_refresh_page (uint8_t page_index) {
  // check if page buffer must be refreshed
  if (sh1106_pagemask & (0x01 << page_index)) {
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (SH1106_ADDR << 1) | I2C_MASTER_WRITE, true);
    i2c_master_write_byte(cmd, SH1106_SINGLE | SH1106_OP_CMD, true);
    i2c_master_write_byte(cmd, SH1106_PAGE | page_index, true);
    i2c_master_write_byte(cmd, SH1106_SINGLE | SH1106_OP_CMD, true);
    i2c_master_write_byte(cmd, SH1106_COLUMN_L + 0x02, true);
    i2c_master_write_byte(cmd, SH1106_SINGLE | SH1106_OP_CMD, true);
    i2c_master_write_byte(cmd, SH1106_COLUMN_H | 0x10, true);
    i2c_master_write_byte(cmd, SH1106_STREAM | SH1106_OP_RAM, true);
    i2c_master_write(cmd, sh1106_buffer[page_index], 128, true);
    i2c_master_stop(cmd);

    i2c_master_cmd_begin(I2C_NUM_0, cmd, 10);
    i2c_cmd_link_delete(cmd);

    sh1106_pagemask ^= (0x01 << page_index);
  }
}


void sh1106_refresh (void) {
  sh1106_refresh_page(0);
  sh1106_refresh_page(1);
  sh1106_refresh_page(2);
  sh1106_refresh_page(3);
  sh1106_refresh_page(4);
  sh1106_refresh_page(5);
  sh1106_refresh_page(6);
  sh1106_refresh_page(7);
}
