
#ifndef SH1106_H
#define SH1106_H

#include <stdint.h>


void sh1106_init ();
void sh1106_contrast (uint8_t contrast);
void sh1106_draw_pixel (uint8_t x, uint8_t y);
void sh1106_draw_hline (uint8_t y);
void sh1106_clear_page (uint8_t page_index);
void sh1106_clear ();
void sh1106_refresh_page (uint8_t page_index);
void sh1106_refresh ();

#endif
